---
layout: layouts/static.njk
title: How to use this resource
---

This website is constructed as a linked resource graph for the sources of
catches, canons and drinking songs, primarily those in the University of
Glasgow’s Euing Collection. The website can be navigated in three ways:
browsing, searching and through RDFa.
<!-- excerpt -->

# Browsing

A good place to start is at the [Songs](/songs/) category which lists all the
songs that have been catalogued in alphabetical order, or the
[Sources](/sources/) category which lists the sources catalogued.

![Screenshot of the “songs” category](/images/static/howto-songs-category.png)

From here, you can click on items such as songs or sources and view the
information for that record. Songs that have an audio recording are marked with
an ![Icon of a tankard](/images/static/recording-icon.png) icon, and songs
that have an edition published on the site are marked with an ![Icon of a piece
of music](/images/static/edition-icon.png) icon. {.content-image-inline}

![Screenshot of a “song” record](/images/static/howto-song-item.png)

The record data is displayed on these pages, and related items are hyperlinked
to their respective records. For example, the composer [John
Lenton](/people/71/) is linked from this page, so is the genre
[“Catch”](/genres/3/), the [number of voices](/voices/3/), the key signature
[D major](/keys/4/), the [time signature](/timesigs/1/), the [edited
text](/texts/273/), where that song has been catalogued ([one](/items/400/),
[two](/items/283/), as well as the facsimile images.

# Searching

Alternatively, records can be queried by the search bar at the top of the page.
Typing text in this search bar will yield the records that match that query.
Clicking on a result will take you to that record’s page.

![Screenshot of searching for “Chloe”](/images/static/howto-search.png)

# RDFa

Finally, the entire website can be explored through linked data, either by
linking to the site through an RDFa–compatible API, or using an RDFa–compatible
browser extension. Alternatively, the entire dataset can be downloaded as JSON
through the [Git
repository](https://gitlab.com/to-your-rude-health/to-your-rude-health-data/-/blob/master/src/tyrh.json).

![Screenshot of an RDFa browser](/images/static/howto-rdf.png)
