const Encore = require('@symfony/webpack-encore');
const config = require('./src/_data/config.json')
const webpack = require('webpack')

let publicPath = '/javascript/';
let prefix = '';
if('subdir' in config) {
	prefix = config.subdir.replace(/^\/+|\/+$/g, "");
	publicPath = ("/" + prefix + publicPath).replace(/\/+/g, "/");
}

Encore
	.setOutputPath('dist/javascript/')
	.setPublicPath(publicPath)
	.setManifestKeyPrefix(prefix)
	.addEntry('index', './src/javascript/index.js')
	.enableSingleRuntimeChunk()
	.enableSourceMaps(!Encore.isProduction())
	.configureBabel(function(bc) {
		bc.plugins.push('@babel/plugin-syntax-jsx');
		bc.plugins.push(['@babel/plugin-transform-react-jsx', {"pragma": "dom" }]);
	}, { })
	.addPlugin(new webpack.DefinePlugin({
		'configSubdir': JSON.stringify(prefix)
	}))
;
if(Encore.isProduction()) {
	Encore
		.cleanupOutputBeforeBuild()
		.enableVersioning()
	;
}
module.exports = Encore.getWebpackConfig();
